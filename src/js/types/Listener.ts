export interface Listener {
  id: string
  action: Function
}