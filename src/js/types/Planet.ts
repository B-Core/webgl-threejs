import { Rotation } from "../classes/Moon";
import { Mesh } from "three";

export interface Planet {
  RADIUS: number
  ROTATION: Rotation
  mesh?: Mesh
}