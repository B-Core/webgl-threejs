import { CubeGeometry, DirectionalLight } from "./lib/three.module";
import { World } from './classes/ThreejsWorld';

const onLoad = function() {
  // Create the container
  const container = document.createElement('div');
  container.classList.add('threejs-container');
  document.body.appendChild(container);

  const world = new World({
    container,
    geometries: [
      {
        geometry: new CubeGeometry(1, 1, 1),
        texture: 'dist/assets/textures/crate_light.jpg',
        mesh: null,
      },
    ],
    lights: [{
      light: new DirectionalLight(
        0xffffff, // color,
        1.5, // opacity
      ),
      position: {
        x: 0,
        y: 0,
        z: 1,
      }
    }]
  });

  world.init();

  window.addEventListener('mousedown', () => {
    world.animating = false;
  });
  window.addEventListener('mouseup', () => {
    world.animating = true;
  });
}

window.addEventListener('DOMContentLoaded', onLoad);