import { Geometry, Mesh } from "three";

export type GeometryObject = {
  geometry: Geometry
  mesh: Mesh
  texture: string
}