import { Light } from "three";

export type LightObject = {
  light: Light
  position: { x: number, y: number, z: number}
}