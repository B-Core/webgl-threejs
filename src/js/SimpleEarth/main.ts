import { SphereGeometry, DirectionalLight } from "./lib/three.module";
import { World } from './classes/ThreejsWorld';
import { GeometryObject } from "./classes/GeometryObject";

const onLoad = function() {
  // Create the container
  const container = document.createElement('div');
  container.classList.add('threejs-container');
  document.body.appendChild(container);

  const geometry = new SphereGeometry(1, 32, 32);
  const texture = 'dist/assets/textures/earth_2.jpg';
  const mesh = null;

  const world = new World({
    container,
    geometries: [
      new GeometryObject(geometry, null, texture, {
        update: function() {
          this.mesh.rotation.y -= 0.01;
        },
        listeners: [
          {
            id: 'mousedown',
            action: function() {
              world.animating = false;
            }
          },
          {
            id: 'mouseup',
            action: function() {
              world.animating = true;
            }
          }
        ]
      }),
    ],
    lights: [{
      light: new DirectionalLight(
        0xffffff, // color,
        1.5, // opacity
      ),
      position: {
        x: 0,
        y: 0,
        z: 1,
      }
    }]
  });

  world.init();
}

window.addEventListener('DOMContentLoaded', onLoad);