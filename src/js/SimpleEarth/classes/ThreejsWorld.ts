import { find } from 'lodash-es/collection';
import { uniqBy } from 'lodash-es/array';
import { defaults } from 'lodash-es/object';
import { WebGLRenderer, CanvasRenderer, Scene, PerspectiveCamera, DirectionalLight, TextureLoader, MeshPhongMaterial, Mesh } from "../lib/three.module";
import { Camera } from 'three';
import { GeometryObject } from "../classes/GeometryObject";
import { LightObject } from "../types/LightObject";
import { Listener } from '../types/Listener';

const DEFAULT_RENDERER_CONFIG = {
  antialias: true,
};

const DEFAULT_CAMERA_CONFIG = [
  45,     // fov
  1,      // near
  4000,   // far
]

export class World {
  container: HTMLElement;
  renderer: WebGLRenderer | CanvasRenderer;
  scene: Scene;
  camera: Camera;
  lights: Array<LightObject>;
  geometries: Array<GeometryObject>;
  listeners: Array<Listener>;
  animating: boolean;
  config: {[key:string]: any};

  constructor(config: {[key:string]: any}) {
    this.config = config;
    this.container = config.container instanceof HTMLElement ? config.container : document.querySelector(config.container);
    
    this.initRenderer();
    
    // set default attributes
    this.scene = null;
    this.lights = this.config.lights || [];
    this.geometries = this.config.geometries || [];
    this.camera = null;
    this.animating = false;
  }

  init() {
    this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
    this.container.appendChild(this.renderer.domElement);

    // create scene
    this.initScene();
    // create camera
    this.initCamera();
    // create light to show off the object
    this.initLights();
    this.initGeometries();

    this.initListeners();

    this.update();
    this.animating = true;
  }

  initRenderer() {
     // create renderer or get config one
     if (!(this.config.renderer instanceof WebGLRenderer) || !(this.config.renderer instanceof CanvasRenderer)) {
      try {
        this.renderer = new WebGLRenderer(defaults((this.config.renderer || {}), DEFAULT_RENDERER_CONFIG));
      } catch(e) {
        console.error(`you'r browser doesn't support webgl canvas`);
        this.renderer = new CanvasRenderer(defaults((this.config.renderer || {}), DEFAULT_RENDERER_CONFIG));
      }
    } else {
      this.renderer = this.config.renderer;
    }
    this.renderer.setPixelRatio( window.devicePixelRatio );
  }

  initScene() {
    this.scene = new Scene();
  }

  initCamera() {
    DEFAULT_CAMERA_CONFIG.splice(1, 0, this.container.offsetWidth / this.container.offsetHeight);
    this.camera = new PerspectiveCamera(...(this.config.camera || DEFAULT_CAMERA_CONFIG));
    this.camera.position.z = 3;
  }

  initLights() {
    this.lights.forEach((l) => {
      l.light.position.set(l.position.x, l.position.y, l.position.z);
      this.scene.add(l.light);
    });
  }

  initGeometries() {
    const textureLoader = new TextureLoader();
    this.geometries.forEach((g) => {
      // load texture for the given geometry
      let map;
      if (g.texture) map = textureLoader.load(g.texture);

      // create a Phong material to it
      const material = new MeshPhongMaterial({
        map,
      });
      const object = new Mesh(g.geometry, material);
      g.mesh = object;
      this.scene.add(g.mesh);
    });
  }

  initListeners() {
    const uniqListeners = uniqBy(this.geometries.reduce((arr, curr) => {
      arr.push(...curr.listeners);
      return arr;
    }, []), 'id');

    uniqListeners.forEach((listener) => {
      window.addEventListener(listener.id, (event) => {
        this.geometries.forEach((g) => {
          const cListener = find(g.listeners, l => l.id === listener.id);
          if (cListener) cListener.action.apply(g, [event]);
        });
      });
    });
  }

  update() {
    this.renderer.render(this.scene, this.camera);

    if (this.animating) {
      this.geometries.forEach(g => g.update());
    }

    window.requestAnimationFrame(this.update.bind(this));
  }
}