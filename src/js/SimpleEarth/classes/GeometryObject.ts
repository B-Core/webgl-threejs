import { Geometry, Mesh } from "three";
import { Listener } from "../types/Listener";

export interface GeometryObject {
  geometry: Geometry
  mesh: Mesh
  texture: string
  listeners: Array<Listener>
  update(): void
}

export class GeometryObject implements GeometryObject {
  constructor(geometry: Geometry, mesh: Mesh, texture: string, params: {[key: string]: any} = {}) {
    this.geometry = geometry;
    this.mesh = mesh;
    this.texture = texture;

    for (let k in params) {
      this[k] = params[k] instanceof Function ? params[k].bind(this) : params[k];
    }
  }

  update() {}
}