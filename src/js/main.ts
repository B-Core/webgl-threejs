import { PointLight } from "./lib/three.module";
import { World } from './classes/ThreejsWorld';
import { GeometryObject } from "./classes/GeometryObject";
import { Earth } from "./classes/Earth";

const onLoad = function() {
  // Create the container
  const container = document.createElement('div');
  container.classList.add('threejs-container');
  document.body.appendChild(container);

  const earth = new Earth();

  const world = new World({
    container,
    geometries: [
      earth,
    ],
    lights: [{
      light: new PointLight(
        0xf1ffcd, // color,
        2,        // intensity
        100,      // size
      ),
      position: {
        x: -20,
        y: 0,
        z: 20,
      }
    }],
    camera: {
      position: {
        z: 7,
      }
    }
  });

  world.init();
}

window.addEventListener('DOMContentLoaded', onLoad);