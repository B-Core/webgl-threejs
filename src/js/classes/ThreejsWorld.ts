import { find } from 'lodash-es/collection';
import { uniqBy } from 'lodash-es/array';
import { defaults } from 'lodash-es/object';
import { WebGLRenderer, CanvasRenderer, Scene, PerspectiveCamera, DirectionalLight, TextureLoader, MeshPhongMaterial, Mesh } from "../lib/three.module";
import { GeometryObject } from "../classes/GeometryObject";
import { LightObject } from "../types/LightObject";
import { Listener } from '../types/Listener';

const DEFAULT_RENDERER_CONFIG = {
  antialias: true,
};

const DEFAULT_CAMERA_CONFIG = {
  fov: 45,
  ratio: 1,
  near: 1,
  far: 4000,
};

const DEFAULT_MOUSE_CONTROLS = {
  start: {
    x: 0,
    y: 0,
  },
  position: {
    x: 0,
    y: 0,
  },
};

const mouse_controls = {
  mousemove: null,
  mouseup: null,
};

export interface MouseControls {
  start: {
    x: number
    y: number
  }
  position: {
    x: number
    y: number
  }
}

export class World {
  container: HTMLElement;
  renderer: WebGLRenderer | CanvasRenderer;
  scene: Scene;
  camera: PerspectiveCamera;
  lights: Array<LightObject>;
  geometries: Array<GeometryObject>;
  listeners: Array<Listener>;
  animating: boolean;
  controls: {
    mouse: MouseControls
  } 
  config: {[key:string]: any};

  constructor(config: {[key:string]: any}) {
    this.config = config;
    this.container = config.container instanceof HTMLElement ? config.container : document.querySelector(config.container);
    
    this.initRenderer();
    
    // set default attributes
    this.scene = null;
    this.lights = this.config.lights || [];
    this.geometries = this.config.geometries || [];
    this.controls = this.config.controls || {
      mouse: DEFAULT_MOUSE_CONTROLS,
    };
    this.camera = null;
    this.animating = false;
  }

  init() {
    this.renderer.setSize(this.container.offsetWidth, this.container.offsetHeight);
    this.container.appendChild(this.renderer.domElement);

    // create scene
    this.initScene();
    // create camera
    this.initCamera();
    // create light to show off the object
    this.initLights();
    this.initGeometries();

    this.initListeners();

    this._update();
    this.animating = true;
  }

  initRenderer() {
     // create renderer or get config one
     if (!(this.config.renderer instanceof WebGLRenderer) || !(this.config.renderer instanceof CanvasRenderer)) {
      try {
        this.renderer = new WebGLRenderer(defaults((this.config.renderer || {}), DEFAULT_RENDERER_CONFIG));
      } catch(e) {
        console.error(`you'r browser doesn't support webgl canvas`);
        this.renderer = new CanvasRenderer(defaults((this.config.renderer || {}), DEFAULT_RENDERER_CONFIG));
      }
    } else {
      this.renderer = this.config.renderer;
    }
    this.renderer.setPixelRatio( window.devicePixelRatio );
  }

  initScene() {
    this.scene = new Scene();
  }

  initCamera() {
    const conf = {
      ...DEFAULT_CAMERA_CONFIG,
      ratio: this.container.offsetWidth / this.container.offsetHeight,
      ...this.config.camera,
    }
    this.camera = new PerspectiveCamera(
      conf.fov,
      conf.ratio,
      conf.near,
      conf.far,
    );
    this.camera.position.x = conf.position && conf.position.x ? conf.position.x : 0;
    this.camera.position.y = conf.position && conf.position.y ? conf.position.y : 0;
    this.camera.position.z = conf.position && conf.position.z ? conf.position.z : 0;
  }

  initLights() {
    this.lights.forEach((l) => {
      l.light.position.set(l.position.x, l.position.y, l.position.z);
      this.scene.add(l.light);
    });
  }

  initGeometries() {
    const textureLoader = new TextureLoader();
    this.geometries.forEach((g) => {
      // load texture for the given geometry
      let map;
      if (g.texture && !g.mesh) {
        map = textureLoader.load(g.texture);

        // create a Phong material to it
        const material = new MeshPhongMaterial({
          map,
        });
        const object = new Mesh(g.geometry, material);
        g.mesh = object;
      }
      this.scene.add(g.group || g.mesh);
    });
  }

  initListeners() {
    const uniqListeners = uniqBy(this.geometries.reduce((arr, curr) => {
      arr.push(...curr.listeners);
      return arr;
    }, []), 'id');

    uniqListeners.forEach((listener) => {
      window.addEventListener(listener.id, (event) => {
        this.geometries.forEach((g) => {
          const cListener = find(g.listeners, l => l.id === listener.id);
          if (cListener) cListener.action.apply(g, [event]);
        });
      });
    });

    if (!this.config.noResize) window.addEventListener('resize', this.onResize.bind(this));
    if (!this.config.noMouseControls) window.addEventListener('mousedown', this.onMouseDown.bind(this));
    if (!this.config.noZoomControl) window.addEventListener('mousewheel', this.onScroll.bind(this));
  }

  onResize() {
    this.camera.aspect = this.container.offsetWidth / this.container.offsetHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( this.container.offsetWidth, this.container.offsetHeight );
  }

  onMouseDown(event) {
    mouse_controls.mousemove = this.onMouseMove.bind(this);
    mouse_controls.mouseup = this.onMouseUp.bind(this);
    window.addEventListener('mousemove', mouse_controls.mousemove);
    window.addEventListener('mouseup', mouse_controls.mouseup);
    this.controls.mouse.start.x = event.clientX;
    this.controls.mouse.start.y = event.clientY;
  }

  onMouseMove(event) {
    this.controls.mouse.position.x = event.clientX;
    this.controls.mouse.position.y = event.clientY;

    const distance = {
      x: this.controls.mouse.start.x - this.controls.mouse.position.x,
      y: this.controls.mouse.start.y - this.controls.mouse.position.y,
    };

    this.camera.position.x += distance.x / 1000;
    this.camera.position.y -= distance.y / 1000;
    this.camera.updateProjectionMatrix();
  }

  onMouseUp(event) {
    window.removeEventListener('mousemove', mouse_controls.mousemove);
    window.removeEventListener('mouseup', mouse_controls.mouseup);
  }

  onScroll(event) {
    this.camera.position.z += event.deltaY / 500;
  }

  _update() {
    this.renderer.render(this.scene, this.camera);

    if (this.animating) {
      this.geometries.forEach(g => g.update());
      this.update();
    }
    window.requestAnimationFrame(this._update.bind(this));
  }

  update(){}
}