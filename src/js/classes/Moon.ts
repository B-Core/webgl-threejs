import { GeometryObject } from './GeometryObject';
import { Planet } from '../types/Planet';
import { SphereBufferGeometry, TextureLoader, MeshPhongMaterial, Color, DoubleSide, Mesh, Object3D } from '../lib/three.module';
import { Earth } from './Earth';

export interface Rotation {
  x: number
  y: number
  z: number
}

export interface MoonParams {
  group?: Object3D
  ROTATION?: Rotation
  TEXTURE_SURFACE?: string
  TEXTURE_NORMAL?: string
  DISTANCE_FROM_EARTH?: number
  PERIOD?: number
  EXAGERATE_FACTOR?: number
  INCLINATION?: number
  RADIUS?: number
}

export interface Moon extends Planet {
  geometry: SphereBufferGeometry
  TEXTURE_SURFACE: string
  TEXTURE_NORMAL: string
  DISTANCE_FROM_EARTH: number;
  PERIOD: number;
  EXAGERATE_FACTOR: number;
  INCLINATION: number;
}

export class Moon extends GeometryObject implements Moon {
  constructor(params: MoonParams = {}) {
    super();
    
    this.listeners = [];
    this.mesh = null;
    this.texture = null;

    this.ROTATION = params.ROTATION || {
      x: 0,
      y: 0.001,
      z: 0,
    };
    this.DISTANCE_FROM_EARTH = params.DISTANCE_FROM_EARTH || 356400; // km
    this.PERIOD = params.PERIOD || 28;
    this.EXAGERATE_FACTOR = params.EXAGERATE_FACTOR || 1.2;
    this.INCLINATION = params.INCLINATION || 0.089;
    this.RADIUS = (params.RADIUS || 1 / 3.7) * this.EXAGERATE_FACTOR;

    this.TEXTURE_SURFACE = params.TEXTURE_SURFACE || 'dist/assets/textures/moon_surface.jpg'; 
    this.TEXTURE_NORMAL = params.TEXTURE_NORMAL || 'dist/assets/textures/moon_normal.jpg'; 

    this.createGlobe();
  }

  createGlobe(): void {
    this.geometry = new SphereBufferGeometry(
      this.RADIUS,        // sphere radius. Default is 50. 
      32,       // number of horizontal segments. Minimum value is 3, and the default is 8. 
      32,       // number of vertical segments. Minimum value is 2, and the default is 6.
   // phiStart — specify horizontal starting angle. Default is 0.
   // phiLength — specify horizontal sweep angle size. Default is Math.PI * 2.
   // thetaStart — specify vertical starting angle. Default is 0.
   // thetaLength — specify vertical sweep angle size. Default is Math.PI.
    );

    const textureLoader = new TextureLoader();

    // create surface, normal
    const surfaceMap = textureLoader.load(this.TEXTURE_SURFACE);
    const normalMap = textureLoader.load(this.TEXTURE_NORMAL);

    const shaderMaterial = new MeshPhongMaterial({
      side: DoubleSide,
      map: surfaceMap,
      normalMap,
    });

    this.mesh = new Mesh(this.geometry, shaderMaterial);
    this.mesh.rotation.z = this.INCLINATION;
  }

  initOrbitingAround(planet: Planet) {
    const distance = this.DISTANCE_FROM_EARTH / planet.RADIUS;

    this.mesh.position.x = Math.sqrt(distance / 2);
    this.mesh.position.z = -Math.sqrt(distance / 2);

    // rotate the Moon so it shows its Moon face toward Earth
    this.mesh.rotation.y = Math.PI;

    this.group = new Object3D();

    this.group.add(this.mesh);
  }

  orbitAround(planet: Planet) {
    this.group.rotation.y += (planet.ROTATION.y / this.PERIOD);
    this.mesh.rotation.y -= this.ROTATION.y;
  }
}