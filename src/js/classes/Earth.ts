import { SphereBufferGeometry, Color, Object3D, TextureLoader, MeshPhongMaterial, Mesh, MeshLambertMaterial, DoubleSide } from "../lib/three.module";
import { GeometryObject } from './GeometryObject';

import { Moon, Rotation } from './Moon';
import { Planet } from "../types/Planet";

const TEXTURE_SURFACE = 'dist/assets/textures/earth_surface_2048.jpg';
const TEXTURE_NORMAL = 'dist/assets/textures/earth_normal_2048.jpg';
const TEXTURE_SPECULAR = 'dist/assets/textures/earth_specular_2048.jpg';
const TEXTURE_CLOUDS = 'dist/assets/textures/earth_clouds_1024.png';

export interface Earth extends Planet {
  geometry: SphereBufferGeometry
  group?: Object3D

  Moon?: Moon

  INCLINATION: number
  CLOUD_SCALE?: number
  CLOUD_ROTATION_FACTOR: number
  CLOUDS_MESH: Mesh
}

export class Earth extends GeometryObject implements Earth {
  constructor() {
    const geometry = new SphereBufferGeometry(1, 32, 32);
    const mesh = null;
    
    super(geometry, null, null, null);
    
    this.listeners = [];
    this.group = new Object3D();

    this.ROTATION = {
      x: 0,
      y: 0.001,
      z: 0,
    };
    this.INCLINATION = 0.41;
    this.RADIUS = 6371; //km
    this.CLOUD_SCALE = 1.005;
    this.CLOUD_ROTATION_FACTOR = 0.8;

    this.createGlobe();
    this.createClouds();
    this.createMoon();
  }

  createGlobe():void {
    const textureLoader = new TextureLoader();

    // create surface, normal and specular map
    const surfaceMap = textureLoader.load(TEXTURE_SURFACE);
    const normalMap = textureLoader.load(TEXTURE_NORMAL);
    const specularMap = textureLoader.load(TEXTURE_SPECULAR);

    const shaderMaterial = new MeshPhongMaterial({
      color: new Color('white'),
      side: DoubleSide,
      specular: 0x222222,
      shininess: 10,
      map: surfaceMap,
      normalMap,
      specularMap,
    });

    this.mesh = new Mesh(this.geometry, shaderMaterial);
    this.mesh.rotation.z = this.INCLINATION;

    this.group.add(this.mesh);
  };
  createClouds():void {
    const textureLoader = new TextureLoader();
    // create clouds
    const cloudMap = textureLoader.load(TEXTURE_CLOUDS);

    const material = new MeshLambertMaterial({
      color: new Color('white'),
      map: cloudMap,
      transparent: true,
      depthWrite: false,
    });

    const geometry = new SphereBufferGeometry(this.CLOUD_SCALE, 32, 32);
    const mesh = new Mesh(geometry, material);

    mesh.rotation.z = this.INCLINATION;

    this.CLOUDS_MESH = mesh;

    // add it to the group
    this.group.add(this.CLOUDS_MESH);
  }
  createMoon(): void {
    this.Moon = new Moon();
    this.Moon.initOrbitingAround(this);
    this.group.add(this.Moon.group);
  }
  update() {
    this.mesh.rotation.y += this.ROTATION.y;
    this.CLOUDS_MESH.rotation.y += this.ROTATION.y * this.CLOUD_ROTATION_FACTOR;
    // this.CLOUDS_MESH.rotation.x -= Math.sin(this.ROTATION.y * this.CLOUD_ROTATION_FACTOR);

    // Moon Orbit around 
    this.Moon.orbitAround(this);
  }
}