export type ShaderType = 'fragment' | 'vertex' | 'tesselation' | 'geometry';

export type ShaderObject = {
  [key in ShaderType]: any
}