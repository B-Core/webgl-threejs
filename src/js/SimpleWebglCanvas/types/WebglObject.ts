export type WebglObject = {
  buffer: WebGLBuffer
  vertSize: number
  nVerts: number
  primtype: number
}