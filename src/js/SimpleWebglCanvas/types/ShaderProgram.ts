export type Attribute = {
  id: string,
  ref: number | null,
}
export type Uniform = {
  id: string,
  value: any,
  ref: number | null,
}

export interface ShaderProgram {
  program: WebGLProgram
  uniforms: Array<{[key:string]: Uniform}>
  attributes: Array<{[key:string]: Attribute}>
}