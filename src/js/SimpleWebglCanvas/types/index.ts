import { CanvasConfiguration } from './CanvasConfiguration';
import { WebglObject } from './WebglObject';
import { MatrixTuple } from './Matrices';
import { ShaderProgram } from './ShaderProgram';
import { ShaderObject, ShaderType } from './ShaderObject';

export {
  CanvasConfiguration,
  WebglObject,
  MatrixTuple,
  ShaderProgram,
  ShaderObject,
  ShaderType,
}