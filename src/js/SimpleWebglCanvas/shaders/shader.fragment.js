export default `
  void main(void) {
    // return the pixel color, always white
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
  }
`;
