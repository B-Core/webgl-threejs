import { ShaderProgram, ShaderObject } from "../types";
import createShader from './createShader';

export default (gl: WebGLRenderingContext, shaders: ShaderObject, uniforms: Array<{[key:string]:any}>, attributes: Array<{[key:string]:any}>): ShaderProgram => {
  // load and compile the fragment and vertex shader
  let fragmentShader, vertexShader;
  if (shaders.fragment) fragmentShader = createShader(gl, shaders.fragment, "fragment");
  if (shaders.vertex) vertexShader = createShader(gl, shaders.vertex, "vertex");
  // link them together into a new program
  const program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);

  // get pointers to the shader params
  attributes.forEach((a) => {
    const attr = gl.getAttribLocation(program, a.id);
    a.ref = gl.enableVertexAttribArray(attr);
  });
  // shaderVertexPositionAttribute = gl.getAttribLocation(program, "vertexPos");
  // gl.enableVertexAttribArray(shaderVertexPositionAttribute);

  uniforms.forEach((u) => {
    u.ref = gl.getUniformLocation(program, u.id);
  });
  
  // shaderProjectionMatrixUniform = gl.getUniformLocation(program, "projectionMatrix");
  // shaderModelViewMatrixUniform = gl.getUniformLocation(program, "modelViewMatrix");
  
  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      alert("Could not initialise shaders");
  }

  return {
    program,
    attributes,
    uniforms,
  }
}