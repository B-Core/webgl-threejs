import { MatrixTuple } from '../types';

export default (): MatrixTuple => {
  // The transform matrix for the square - translate back in Z 
  // for the camera 
  const modelViewMatrix = new Float32Array([
    1, 0, 0, 0, 
    0, 1, 0, 0, 
    0, 0, 1, 0, 
    0, 0, -3.333, 1
  ]); 
  // The projection matrix (for a 45 degree field of view) 
  const projectionMatrix = new Float32Array([
    2.41421, 0, 0, 0, 
    0, 2.41421, 0, 0, 
    0, 0, -1.002002, -1, 
    0, 0, -0.2002002, 0
  ]);

  return [
    modelViewMatrix,
    projectionMatrix,
  ];
}