import { CanvasConfiguration } from '../types';

export default (params: CanvasConfiguration): HTMLCanvasElement => {
  const canvas = document.createElement('canvas');
  if (params.class) canvas.className = params.class;
  if (params.id) canvas.id = params.id;
  return canvas;
}