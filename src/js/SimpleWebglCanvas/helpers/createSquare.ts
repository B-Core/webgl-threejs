import { WebglObject } from '../types';

// creating vertext data for square to be drawn

export default (gl: WebGLRenderingContext): WebglObject => {
  let vertexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);

  let vertices = [
    .5, .5, .0,
    -.5, .5, .0,
    .5, -.5, .0,
    -.5, -.5, .0,
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

  const square = {
    buffer: vertexBuffer,
    vertSize: 3,
    nVerts: 4,
    primtype: gl.TRIANGLE_STRIP,
  };

  return square;
}