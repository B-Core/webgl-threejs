import { find } from 'lodash-es/collection';
import { WebglObject, ShaderProgram } from "../types";

export default (shaderProgram: ShaderProgram, gl: WebGLRenderingContext, obj: WebglObject): void => {
  // clear the background with black
  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT);

  // set the vertex buffer to be drawn
  gl.bindBuffer(gl.ARRAY_BUFFER, obj.buffer);

  // set the shader to use
  gl.useProgram(shaderProgram.program);

  // connect up the shader parameters vertex position 
  // and projection/matrix matrices

  shaderProgram.attributes.forEach((a) => {
    const k = Object.keys(a);
    gl.vertexAttribPointer(a[k[0]].ref, obj.vertSize, gl.FLOAT, false, 0, 0);
  });
  
  
  const u_projectionMatrix = find(shaderProgram.uniforms, { id: 'projectionMatrix' });
  const u_modelViewMatrix = find(shaderProgram.uniforms, { id: 'modelViewMatrix' });
  gl.uniformMatrix4fv(u_projectionMatrix.ref, false, u_projectionMatrix.value);
  gl.uniformMatrix4fv(u_modelViewMatrix.ref, false, u_modelViewMatrix.value);

  // draw the object
  gl.drawArrays(obj.primtype, 0, obj.nVerts);
}