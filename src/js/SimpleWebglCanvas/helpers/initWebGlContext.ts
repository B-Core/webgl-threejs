export default (canvas: HTMLCanvasElement): WebGLRenderingContext => {
  let gl;
  try {
    gl = canvas.getContext('experimental-webgl');
  } catch (e) {
    const msg = "Error creating WebGL Context!: " + e.toString();
    alert(msg);
    throw new Error(msg);
  }
  return gl;
}