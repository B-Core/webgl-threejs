export default (gl: WebGLRenderingContext, str: string, type: string): WebGLShader => {
  let shader;
  if (type === "fragment") {
      shader = gl.createShader(gl.FRAGMENT_SHADER);
  } else if (type === "vertex") {
      shader = gl.createShader(gl.VERTEX_SHADER);
  } else {
      return null;
  }
  gl.shaderSource(shader, str);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert(gl.getShaderInfoLog(shader));
      return null;
  }
  return shader;
}