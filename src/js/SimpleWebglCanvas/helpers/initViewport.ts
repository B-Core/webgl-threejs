export default (gl: WebGLRenderingContext, canvas: HTMLCanvasElement): void => {
  gl.viewport(0, 0, canvas.width, canvas.height);
}