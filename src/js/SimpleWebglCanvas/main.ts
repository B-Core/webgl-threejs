import createCanvasElement from './helpers/createCanvasElement';
import initWebGlContext from './helpers/initWebglContext';
import initViewport from './helpers/initViewport';
import initMatrices from './helpers/initMatrices';
import initShader from './helpers/initShader';
import createSquare from './helpers/createSquare';
import draw from './helpers/draw';

import vertexShaderSource from './shaders/shader.vertex';
import fragmentShaderSource from './shaders/shader.fragment';

const init = () => {
  const canvas = createCanvasElement({
    id: 'three-canvas',
  });
  const gl = initWebGlContext(canvas); 
  initViewport(gl, canvas);
  
  document.body.appendChild(canvas);

  const rawShaders = {
    vertex: vertexShaderSource,
    fragment: fragmentShaderSource,
    tesselation: undefined,
    geometry: undefined,
  };
  const matrices = initMatrices();
  const uniforms = [{
    id: 'modelViewMatrix',
    value: matrices[0],
    ref: null,
  }, {
    id: 'projectionMatrix',
    value: matrices[1],
    ref: null,
  }];
  const attributes = [{
    id: "vertexPos",
    ref: null,
  }, ];
  const squares = createSquare(gl);
  const shaderProgram = initShader(gl, rawShaders, uniforms, attributes);
  draw(shaderProgram, gl, squares);
}

window.addEventListener('DOMContentLoaded', init);